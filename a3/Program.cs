﻿using System;

namespace a3
{
    public class Program
    {
        public static void Main(string[] args)
        {
	   
		
            Console.WriteLine("Program Requirements: ");
	    Console.WriteLine("A3 - Future Value Calculator");
	    Console.WriteLine("Author: Ryan Lindholm");
	    Console.WriteLine("1) Use intrinsic method to display date/time:");
	    Console.WriteLine("2) Research what is future value? And, its formula");
	    Console.WriteLine("3) Create FutureValue method using the following parameters: decimal presentValue, decimal yearlyInt, decimal MonthlyDep");
	    Console.WriteLine("4) Intialize suitable variable(s): use decimal data types for currency variables");
	    Console.WriteLine("5) Perform data validation: prompt user until correct data is entered");
	    Console.WriteLine("6) Display money in currency format");
	    Console.WriteLine("7) Allow user to press any key to return back to command line");
	    

	    DateTime date = DateTime.Now;

	    Console.WriteLine("Now " + date);

	    
	    Console.Write("Starting Balance: ");
	    string input = Console.ReadLine();
	    decimal presentValue; 
	    while (!(Decimal.TryParse(input, out presentValue)))
	    {	 
	    	 Console.WriteLine("Starting balance must be numeric");
		 Console.Write("Starting Balance: ");
		 input = Console.ReadLine();
	    }


	    int numYears;
	    Console.Write("\nTerm (years): ");
	    input = Console.ReadLine();
	    while (!(int.TryParse(input, out numYears)))
	    {
		Console.WriteLine("Term must be integer data type.");
		Console.Write("Term (years): ");
		input = Console.ReadLine();
	    }

	    
	    decimal yearlyInt;
	    Console.Write("\nInterest Rate: ");
	    input = Console.ReadLine();
	    while (!(Decimal.TryParse(input, out yearlyInt)))
	    {
		Console.WriteLine("Interest rate must be numeric.");
		Console.Write("Interest Rate: ");
		input = Console.ReadLine();
	    }


	    decimal monthlyDep;
	    Console.Write("\nDeposit (monthly): ");
	    input = Console.ReadLine();
	    while (!(Decimal.TryParse(input, out monthlyDep)))
	    {
		Console.WriteLine("Monthly deposit must be numeric");
		Console.Write("Deposit (monthly); ");
		input = Console.ReadLine();
	    }
	    
	    FutureValue(presentValue, numYears, yearlyInt, monthlyDep);
	    Console.Write("Press any key to continue");
	    Console.ReadKey();
        }

	public static void FutureValue (decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep)
	{ 

		int numMonths = numYears * 12;
		double rate = (double)yearlyInt/12/100;

		decimal answer = presentValue * (decimal)Math.Pow((1 + rate), numMonths) + (monthlyDep * (decimal)(Math.Pow((1 + rate), numMonths) - 1) / (decimal)rate); 
		string currency = String.Format(" {0:C}", answer);
		Console.WriteLine("*** Future Value: ***");
		Console.WriteLine(currency);
	}


    }
}
