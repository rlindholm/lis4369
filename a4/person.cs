using System;

namespace a4
{
	public class Person
	{
		protected string fname;
		protected string lname;
		protected int age;

		public Person()
		{
			fname = "John";
			lname = "Doe";
			age = 0;
			
			Console.WriteLine("Creating base person object from default constructor (accepts no arguments): ");

		}
		
		public Person(string fn="", string ln="", int a=0)
		{
			fname = fn;
			lname = ln;
			age = a;
			Console.WriteLine("Creating base person object from within paramatized constructor");

		}

		public void SetFName (string fn="")
		{
			fname = fn;
		}
		public void SetLName (string ln="")
		{
			lname = ln;
		}
		public void SetAge (int a=0)
		{
			age = a;
		}


		public string GetFName ()
		{
			return fname;
		}
		public string GetLName ()
		{
			return lname;
		}
		public int GetAge ()
		{
			return age;
		}
		public virtual string GetObjectInfo ()
		{      
			return this.GetFName() + " " + this.GetLName() + " is " + this.GetAge();		
		}
		public virtual void Display()
		{
			Console.WriteLine("First Name: " + this.GetFName() + "\nLast Name: " + this.GetLName() + "\nAge: " + this.GetAge());
		}

	}
	public class Student : Person
	{
		private string college, major;
		private double gpa;

		public Student()
		{      
		       Console.WriteLine("Creating derived student object from default constructor (accepts no arguments)");
			college = "Default college";
			major = "Default major";
			gpa = 0.00f;
		}
		public Student(string fn, string ln, int a, string c, string m, double g)
		: base(fn,ln,a)
		{
			college = c;
			major = m;
			gpa = g;
			Console.WriteLine("Creating derived student object from within the paramaterized constructor (accepts 6 arguments)");
		}
		
		public string GetName()
		{
			return fname;
		}
		public string GetFullName()
		{
			return fname + lname;
		}
		public override string GetObjectInfo()
		{
			return base.GetObjectInfo() + " is in the college of " + college + " and is majoring in " + major + " with a gpa of " + gpa.ToString("F2") + ".";
		}
		public override void Display()
		{
			base.Display();
			Console.WriteLine("College: " + college + "\nMajor: " + major);
			Console.WriteLine("GPA: " + (gpa.ToString("F2")));
		}
		
	}


}