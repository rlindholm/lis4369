﻿using System;

namespace a4
{
    public class Program
    {
        public static void Main(string[] args)
        {
		
	    Console.WriteLine("\nTitle: A4 - Using Class Inheritance");
	    Console.WriteLine("Author: Ryan Lindholm");
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss tt\n"));
	    Person p1 = new Person();
	    p1.Display();

	    Console.Write("\nModify person objects data member values created from default constructor(accepts no argumetns)");

	    Console.Write("\nFirst Name: ");
	    string fname = Console.ReadLine();
	    Console.Write("Last Name: ");
	    string lname = Console.ReadLine();
	    Console.Write("Age: ");
	    int age;
	    while (!int.TryParse(Console.ReadLine(), out age))
	    {
		Console.Write("Invalid data type, please enter an int: ");
	    }

	    p1.SetFName(fname);
	    p1.SetLName(lname);
	    p1.SetAge(age);
	    

	    Console.WriteLine("\nDisplay object's new data member values: ");
	    
	    p1.Display();

	    Console.WriteLine("\nCall parameterized constructor (accepts three arguments)");
	    Console.Write("First Name: ");
	    fname = Console.ReadLine();
	    Console.Write("Last Name: ");
	    lname = Console.ReadLine();
	    Console.Write("Age: ");
	    while (!int.TryParse(Console.ReadLine(), out age))
	    {
		Console.Write("Invalid data type, please enter an int: ");
	    }
	    Console.Write("\n");
	    
	    Person p2 = new Person(fname, lname, age);
	    p2.Display();

	    Console.WriteLine("\nCall derived default constructor (inherits from base class)\n");
	    
	    Student s1 = new Student();
	    s1.Display();	    
	   
	    Console.WriteLine("\nDemonstrating Polymorphism (new derived object);");

	    Console.Write("First Name: ");
	    fname = Console.ReadLine();
	    Console.Write("Last Name: ");
	    lname = Console.ReadLine();
	    Console.Write("Age: ");
	    while (!int.TryParse(Console.ReadLine(), out age))
	    {
		Console.Write("Invalid data type, please enter an int: ");
	    }
	    Console.Write("College: ");
	    string college = Console.ReadLine();
	    Console.Write("Major: ");
	    string major = Console.ReadLine();
	    Console.Write("GPA: ");
	    double gpa;
	    while (!double.TryParse(Console.ReadLine(), out gpa))
	    {
		Console.Write("Invalid data type, please enter a double: ");
	    }
	    Console.Write("\n");
	    
	    Student s2 = new Student (fname, lname, age, college, major, gpa);

	    Console.WriteLine("\nperson2 - GetObjectInfo (virtual):");
	    string info = p2.GetObjectInfo();
	    Console.WriteLine(info);

	    Console.WriteLine("\nstudent2 - GetObjectInfo (overridden):");
	    info = s2.GetObjectInfo();
	    Console.WriteLine(info);
	    
	    Console.WriteLine("Press any key to exit program");
	    Console.ReadKey();
	    	    

        }
    }
}
