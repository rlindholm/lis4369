# LIS4369  

#Assignment 4 Requirements:  
Five Parts  

1. Display short assignment requirements  
2. Display your name as author  
3. Display current date/time (must include date/time, your format preference)  
4. Create two classes: person and student (see fields and methods below)  
5. Must include data validation on numeric data.  

Screenshots  
![Valid Operation: ](http://i.imgur.com/TIBWWSt.png)  
![Valid Operation: ](http://i.imgur.com/7UoQ6dG.png)  

