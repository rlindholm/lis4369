﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            Decimal bill,
                twoBookDiscount,
                threeToFiveBookDiscount,
                greaterThanSixBookDiscount;
            
            int booksBought;

            Console.WriteLine("Project Requirements: ");
            Console.WriteLine("1) Use intrinsic method to display date/time");
            Console.WriteLine("2) Use two types of selection structures: if...else if... else, and switch");
            Console.WriteLine("3) Initialize suitable variable(s): use decimal data type for bill");
            Console.WriteLine("4) Prompt and capture user input");
            Console.WriteLine("5) Display money in currency format");
            Console.WriteLine("6) Allow user to press any key to return back to command line");

            DateTime localDate = DateTime.Now;

            Console.WriteLine(localDate);


            Console.WriteLine("******if...else if...else example*****");
            Console.WriteLine("What is your bill: ");
            bill = decimal.Parse(Console.ReadLine());

            Console.WriteLine("Your bill is $" + bill);


            Console.WriteLine("Discounts:");
            Console.WriteLine("6+: 30% off");
            Console.WriteLine("3-5: 20% off");
            Console.WriteLine("2: 10% off");
            Console.WriteLine("1: Regular price");

            Console.WriteLine("How many books did you buy? ");
            booksBought = int.Parse(Console.ReadLine());

            if (booksBought == 1)
                Console.WriteLine("Your cost: $" + bill);
 
            else if (booksBought == 2)
                {
                    twoBookDiscount = Convert.ToDecimal(.90);
                    Console.WriteLine("Your cost: $" + decimal.Multiply(bill, twoBookDiscount));
                }
            else if (booksBought > 2 && booksBought < 6)
                {
                    threeToFiveBookDiscount = Convert.ToDecimal(.80);
                    Console.WriteLine("Your Cost: $" + decimal.Multiply(bill, threeToFiveBookDiscount));
                }
            else if (booksBought > 6)
                {
                    greaterThanSixBookDiscount = Convert.ToDecimal(.70);
                    Console.WriteLine("Your Cost: $" + decimal.Multiply(bill, greaterThanSixBookDiscount));
                }
             else
                    Console.WriteLine("Invalid number of books");


            Console.WriteLine("*****switch example*****");

            Console.WriteLine("1 - red");
            Console.WriteLine("2 - green");
            Console.WriteLine("3 - blue");

            Console.WriteLine("What is your favorite color?");
            int choice = 0;

            choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.WriteLine("Your favorite color is red");
                    break;
                case 2:
                    Console.WriteLine("Your favorite color is green");
                    break;
                case 3:
                    Console.WriteLine("Your favorite color is blue");
                    break;
            }

	    Console.WriteLine("Press any key to return to command line");
            Console.ReadKey();





        }
    }
}
