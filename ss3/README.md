# Skillset 3

##Program requirements

1) Use intrinsic method to display date/time;  
2) Use two types of selection structures: if...else if...else, and switch;  
3) Initialize suitable variable(s): use decimal data type for bill;  
4) Prompt and capture user input;  
5) Display money in currency format;    
6) Allow user to press any key to return back to command line;    

*Screenshot*  
![skillset3](http://i.imgur.com/fJpEd3z.png)