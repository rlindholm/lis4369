# LIS4369

##Assignment 2 Requirements:
Four Parts

1. Display short assignment requirements
2. Display Ryan Lindholm as the author
3. Display current date/time (must include date/time your format preference)
4. Must perform and display each mathematical operation

![Valid Operation: ](http://i.imgur.com/y0Vao8p.png)

![Invalid Operation: ](http://i.imgur.com/cOl31K0.png)