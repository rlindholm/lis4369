﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A2: Simple Calculator");
            Console.WriteLine("Note: Program does not perform data validation.");
            Console.WriteLine("Author: Charles Ryan Lindholm");
            DateTime date = DateTime.Now;
            Console.WriteLine("Now: " + date);

            Console.Write("num1: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("num2: ");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplication");
            Console.WriteLine("4 - Division");

            Console.Write("Choose a mathematical operation: ");
            int choice = int.Parse(Console.ReadLine());

            if (choice == 1)
            {
                Console.WriteLine("*** Result of Addition operation: ***");
                int addition = num1 + num2;
                Console.WriteLine(addition);
            }
            else if (choice == 2)
            {
                Console.WriteLine("*** Result of Subtraction operation: ***");
                int subtraction = num1 - num2;
                Console.WriteLine(subtraction);
            }
            else if (choice == 3)
            {
                Console.WriteLine("*** Result of Multiplication operation: ***");
                int multiplication = num1 * num2;
                Console.WriteLine(multiplication);
            }
            else if (choice == 4)
            {
                while (num1 == 0 || num2 == 0)
                {
                    Console.Write("Division by zero is not permitted please enter a new number: ");
                    if (num1 == 0)
                        num1 = int.Parse(Console.ReadLine());
                    else
                        num2 = int.Parse(Console.ReadLine());
                }
                Console.WriteLine("*** Result of Division operation: ***");
                double division = (double)num1 / (double)num2;
                Console.WriteLine(division);
            }
            else
                Console.WriteLine("Invalid choice");
	    Console.WriteLine("Press any key to return to the command line");
            Console.ReadKey();
        }
        
    }
}
