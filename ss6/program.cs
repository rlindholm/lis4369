﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Title: Non-Value Returning (void) Functions");
            Console.WriteLine("Author: Charles Ryan Lindholm");
            DateTime date = DateTime.Now;
            Console.WriteLine("Now: " + date);

            Console.Write("num1: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("num2: ");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("\n1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplication");
            Console.WriteLine("4 - Division");
	    Console.WriteLine("5 - Exponentiation");

            Console.Write("Choose a mathematical operation: ");
            int choice = int.Parse(Console.ReadLine());

            if (choice == 1)
            {
		int add = Addition(num1, num2);
		Console.WriteLine("*** Result of Addition operation ***");
		Console.WriteLine(add);
            }
            else if (choice == 2)
            {
		int subtract = Subtraction(num1, num2);
		Console.WriteLine("*** Result of Subtraction operation ***");
		Console.WriteLine(subtract);
            }
            else if (choice == 3)
            {
		int multiply = Multiplication(num1, num2);
		Console.WriteLine("*** Result of Multiplication operation ***");
		Console.WriteLine(multiply);
            }
            else if (choice == 4)
	    {
		double divide = Division (num1, num2);
		Console.WriteLine("*** Result of Division operation ***");
		Console.WriteLine(divide);

            }
	    else if (choice == 5)
	    {
		double power = Exponentiation (num1, num2);
		Console.WriteLine("*** Result of Exponentiation operation ***");
		Console.WriteLine(power);

	    }
            else
                Console.WriteLine("Invalid choice");
	    Console.WriteLine("Press any key to return to the command line");
            Console.ReadKey();
        }
	
	public static int Addition (int num1, int num2)
	{
		int add = num1 + num2;
		return add;
        }
	public static int Subtraction (int num1, int num2)
	{
		int subtract = num1 - num2;
		return subtract;
	}
	public static int Multiplication (int num1, int num2)
	{
                int multiplication = num1 * num2;
		return multiplication;
        }
	public static double Division (int num1, int num2)
	{
             while (num1 == 0 || num2 == 0)
             {
                 Console.Write("Division by zero is not permitted please enter a new number: ");
                 if (num1 == 0)
                     num1 = int.Parse(Console.ReadLine());
                 else
                     num2 = int.Parse(Console.ReadLine());
                }
             double division = (double)num1 / (double)num2;
	     return division;
	}
	public static double Exponentiation (int num1, int num2)
	{
		double exponentiation = Math.Pow((double)num1, (double)num2);
		return exponentiation;
	}

    }
}
