using System;

namespace p1
{
	public class Room
	{
	   private string type;
	   private double length;
	   private double width;
	   private double height;

	   public Room()
	   {
		type = "Default";
		length = 0.0;
		width = 0.0;
		height = 0.0;
	   }
	   
	   public Room(string t, double l, double w, double h)
	   {
		type = t;
		length = l;
		width = w;
		height = h;
	   }


	   public void SetType(string t)
	   {
		type = t;
	   }

	   public void SetLength(double l)
	   {
		length = l;
	   }
	   public void SetWidth(double w)
	   {
		width = w;
	   }
	   public void SetHeight(double h)
	   {
		height = h;
	   }


	   public new string GetType()
	   {
		return type;
	   }
	   public double GetLength()
	   {
		return length;
	   }
	   public double GetWidth()
	   {
		return width;
	   }
	   public double GetHeight()
	   {
		return height;
	   }
	   

	   public double GetArea()
	   {
		return length*width;
	   }
	   public double GetVolume()
	   {
		return length*width*height;
	   }

	   public void Display()
	   {
		Console.WriteLine("Room Type: " + this.GetType());
		Console.WriteLine("Room Length: " + this.GetLength());
		Console.WriteLine("Room Width: " + this.GetWidth());
		Console.WriteLine("Room Height: " + this.GetHeight());


		Console.WriteLine("Room Area: " + (this.GetArea()).ToString("F2") + " sq ft ");
		Console.WriteLine("Room Volume: " + (this.GetVolume()).ToString("F2") + " cu ft ");
	   }

	}

}
