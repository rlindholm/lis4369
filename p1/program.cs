﻿using System;

namespace p1
{
    public class Program
    {
        public static void Main(string[] args)
        {   
	    string requirements = @"//////////////////////////////
Program Requirements:
P1 - Room Size Calculator using Classes
Author: Ryan Lindholm
	1) Create Room Class
	2) Create following fields (aka properties or data member)
	   a. private string type
	   b. private double length
	   c. private double width
	   d. private double height
	3) Create two constructors
	   a. Default constructor
	   b. Parameterized constructor that accepts four arguments (for fields above
	4) Create the following mutator (aka setter) methods
	   a. SetType
	   b. SetLength
	   c. SetWidth
	   d. SetHeight
	5) Create the following accessor (aka getter methods
	   a. GetType
	   b. GetLength
	   c. GetWidth
	   d. GetHeight
	   e. GetArea
	   f. GetVolume
	6) Must include the following functionality:
	   a. Display room size calculations in feet (as per below)
	   b. Must include data validation
	   c. Round to two decimal places
	7) Allow user to press any key to return back to command line
/////////////////////////////////////////";
	    
	    
	    Console.Write(requirements);
	    Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss tt\n"));

	    Console.WriteLine("Creating Default room object from default constructor (accepts no arguments)");
	    
	    Room room1 = new Room();

	    room1.Display();

	    Console.WriteLine("\nModify default room object's data member values");
	    Console.WriteLine("Use setter/getter methods:");
	    
	    Console.Write("Room type: ");
	    string type = Console.ReadLine();
	    room1.SetType(type);

	    double length,
	    	   width,	
		   height;

	    Console.Write("Room Length: ");
	    
	    while (!double.TryParse(Console.ReadLine(), out length))
	    {
		Console.Write("Invalid data type, please enter a double: ");
	    }
	    room1.SetLength(length);

	    Console.Write("Room Width: ");
	    while (!double.TryParse(Console.ReadLine(), out width))
	    {
		Console.Write("Invalid data type, please enter a double: ");

	    }
	    room1.SetWidth(width);

	    Console.Write("Room Height: ");
	    while (!double.TryParse(Console.ReadLine(), out height))
	    {
		Console.Write("Invalid data type, please enter a double: ");
	    }
	    room1.SetHeight(height);

	    Console.WriteLine("\nDisplaying first object after mutator functions");
	    room1.Display();
	    

	    Console.WriteLine("\nCreating room object from parameterized constructor");
	    
	    Console.Write("Room Type: ");
	    type = Console.ReadLine();

	    Console.Write("Room Length: ");
	    while (!double.TryParse(Console.ReadLine(), out length))
	    {
		Console.Write("Invalid data type, please enter a double: ");
	    }

	    Console.Write("Room Width: ");
	    while (!double.TryParse(Console.ReadLine(), out width))
	    {
		Console.Write("Invalid data type, please enter a double: ");

	    }

	    Console.Write("Room Height: ");
	    while (!double.TryParse(Console.ReadLine(), out height))
	    {
		Console.Write("Invalid data type, please enter a double: ");
	    }
	    
	    Room room2 = new Room(type, length, width, height);
	    
	    Console.WriteLine("\nDisplaying the new parameterized constructor values: ");	    
	    room2.Display();
	    
	    Console.WriteLine("Press any key to exit");
	    Console.ReadKey();
	  
        }
    }
}
