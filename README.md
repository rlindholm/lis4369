# LIS4369
## Extensible Enterprise Solutions
### Ryan Lindholm


[A1 README.md](https://bitbucket.org/rlindholm/lis4369/src/357b016051aa5410d76e7e0658a7eb45b39e113d/a1/README.md?at=master&fileviewer=file-view-default)

- Distributed Version Control with Git and Bitbucket  
- Development Installations  
- Questions  

[A2 README.md](https://bitbucket.org/rlindholm/lis4369/src/97e085a2874c2a2a2453a7fd2afc86502bc9d6e2/a2/README.md?at=master&fileviewer=file-view-default)

- Dislay short assignment requirements
- Display your name as "author"
- Display current date/time
- Must Perform and Display each mathematical operation


[A3 README.md](https://bitbucket.org/rlindholm/lis4369/src/32954bd81971944cfba9de679a7b23b68c4bac3e/a3/README.md?at=master&fileviewer=file-view-default)

- Display short assignment requirements
- Display your name as "author"
- Display current date/time (must include date/time your format preference)
- Must display future value calculation must include data validation, use decimal data type for currency variables and use currency formatting  


[P1 README.md](https://bitbucket.org/rlindholm/lis4369/src/caa9088e3734ca80e11621e9f7cd781f59488246/p1/readme.md?at=master&fileviewer=file-view-default)

- Display short assignment requirements
- Display your name as "author"
- Display current date/time (must include date/time your format preference
- Must perform and display room size calculations, must include data validation and rounding to two decimal places
- Each data member must have get/set methods, also GetArea and Get Volume

[A4 README.md](https://bitbucket.org/rlindholm/lis4369/src/6a362ff8058548a64c682c6e5e53e70a8df81ca9/a4/readme.md?at=master&fileviewer=file-view-default)

- Display short assignment requirements
- Display your name as "author"
- Display current date/time (must include date/time your format preference
- Create two classes: person and student (see fields and methods below)  
- Must include data validation on numeric data  

[A5 README.md](https://bitbucket.org/rlindholm/lis4369/src/ea319ee2a1004e095aeaa5f0eeedf035a546d1e8/a5/readme.md?at=master&fileviewer=file-view-default)

- Display short assignment requirements
- Display your name as "author"
- Display current date/time (must include date/time your format preference
- Create two classes: car and vehicle    
- Must include data validation on numeric data  

[p2 README.md](https://bitbucket.org/rlindholm/lis4369/src/176d55a6436057f06f94e3368187164ee8b7f844/p2/readme.md?at=master&fileviewer=file-view-default)

- Display assignment requirements  
- Display your name as author  
- Display current date/time  
- Work your way throuhg the tutorial  
- Create the required searches  

[SS1 README.md](https://bitbucket.org/rlindholm/lis4369/src/73b88ab1340f9cccf1e2a5c3ae7946556dfb723d/ss1/README.md?at=master&fileviewer=file-view-default)  

- Hello world  


[SS2 README.md](https://bitbucket.org/rlindholm/lis4369/src/adfdd7eb16786f3b8463e1aad5b35f5f7672f668/ss2/README.md?at=master&fileviewer=file-view-default)  

- Fahrenheit to celsius and back converter  


[SS3 README.md](https://bitbucket.org/rlindholm/lis4369/src/adfdd7eb16786f3b8463e1aad5b35f5f7672f668/ss3/README.md?at=master&fileviewer=file-view-default)  

- Testing control statements  
- Calculating discount and selecting color using if else and switch  


[SS4 README.md](https://bitbucket.org/rlindholm/lis4369/src/adfdd7eb16786f3b8463e1aad5b35f5f7672f668/ss4/README.md?at=master&fileviewer=file-view-default)  

- Testing looping structures    


[SS5 README.md](https://bitbucket.org/rlindholm/lis4369/src/adfdd7eb16786f3b8463e1aad5b35f5f7672f668/ss5/README.md?at=master&fileviewer=file-view-default)  

- Mathematical operations  
- Manipulating User input with void functions  

[SS6 README.md](https://bitbucket.org/rlindholm/lis4369/src/adfdd7eb16786f3b8463e1aad5b35f5f7672f668/ss6/README.md?at=master&fileviewer=file-view-default)  

- Mathematical operations  
- Value returning functions  

[SS7 README.md](https://bitbucket.org/rlindholm/lis4369/src/49a41676f9f610b5294d784c8993f97ac865f4f4/ss7/README.md?at=master&fileviewer=file-view-default)  

- Creating a class using a default and paramaterized constructor
- Modify objects member data using default and paramaterized constructor  
