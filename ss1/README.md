# Skillset 1

##Program requirements

1) Initialize suitable variables(s);  
2) Prompt and capture user input;  
3) Display user input;  
4) Allow user to press any key to return back to command line;  

*Screenshot*
![skillset1](http://i.imgur.com/lPDxfvE.png)