﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            Console.WriteLine("Project Requirements: ");
            Console.WriteLine("1) Initialize suitable variable(s)");
            Console.WriteLine("2) Prompt and capture user input;");
            Console.WriteLine("3) Display user input: ");
            Console.WriteLine("4) Allow user to press any key to return back to the command line");

            Console.WriteLine("Please enter your full name: ");
            name = Console.ReadLine();

            Console.WriteLine("Hello " + name);
	    
	    Console.WriteLine("Press any key to return to the command line");
            Console.ReadKey();
        }
    }
}
