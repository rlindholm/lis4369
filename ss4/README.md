# Skillset 4

##Program requirements

1) Use intrinsic method to display date/time;  
2) Use four types of iteration structures: for, while, do...while, foreach;  
3) Initialize suitable variable(s);  
4) Loop through numbers 1-10;  
5) Create string array (for days of week)  
6) Use foreach iteration structure to loop through days of week;  
7) Allow user to press any key to return back to command line;  

*Screenshot*
![skillset4](http://i.imgur.com/HUs2Vub.png)