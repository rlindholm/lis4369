﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Project Requirements: ");
            Console.WriteLine("1) Use intrinsic method to display date/time");
            Console.WriteLine("2) Use four types of iteration structures: for, while, do...while, foreach");
            Console.WriteLine("3) Initialize suitable variable(s)");
            Console.WriteLine("4) Loop through numbers 1-10");
            Console.WriteLine("5) Create string array (for days of week");
            Console.WriteLine("6) Use foreach iteration structure to loop through days of week");
            Console.WriteLine("7) Allow user to press any key to return back to command line.");


            DateTime localDate = DateTime.Now;

            Console.WriteLine("Now: " + localDate);

            const int COUNTER = 10;

            Console.WriteLine("for loop: ");
            for (int i = 1; i <= COUNTER; i++)
                Console.Write(i + " ");

            int number = 1;
            Console.WriteLine("\nwhile loop: ");
            while (number <= 10)
            {
                Console.Write(number + " ");
                number++;
            }

            int doWhile = 1;
            Console.WriteLine("\ndo...while loop: ");
            do
            {
                Console.Write(doWhile + " ");
                doWhile++;

            } while (doWhile <= 10);

            string[] daysOfWeek = new string[7] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

            Console.WriteLine("\nfor each loop: ");
            foreach (string day in daysOfWeek)
                Console.Write(day + " ");

	    Console.WriteLine("\nPress any key to return to command line");	
            Console.ReadKey();

        }
    }
}
