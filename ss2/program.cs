﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            double fahrenheit,
                 celsiusFromFahrenheit,
                 celsius,
                 fahrenheitFromCelsius;

            const double NINEOVERFIVE = .55;
        
            Console.WriteLine("Project Requirements: ");
            Console.WriteLine("1) Initialize suitable variable(s)");
            Console.WriteLine("2) Prompt and capture user input;");
            Console.WriteLine("3) Convert into Fahrenheit and Celsius");
            Console.WriteLine("4) Allow user to press any key to return back to the command line");

            Console.WriteLine("Fahrenheit temperature: ");
            fahrenheit = int.Parse(Console.ReadLine());
            celsiusFromFahrenheit = ((fahrenheit - 32) * NINEOVERFIVE);
            Console.WriteLine(fahrenheit + "F = " + celsiusFromFahrenheit + "C");

    

            Console.WriteLine("Celcius temperature: ");
            celsius = int.Parse(Console.ReadLine());
            fahrenheitFromCelsius = ((celsius * NINEOVERFIVE) + 32);
            Console.WriteLine(celsius + "C = " + fahrenheitFromCelsius + "F");

	    Console.WriteLine("Press any key to return to the command line");
            Console.ReadKey();

        }
    }
}
