# Skillset 2

##Program requirements

1) Initialize suitable variables(s);  
2) Prompt and capture user input;  
3) Convert into Fahrenheit and Celsius;  
4) Allow user to press any key to return back to command line;  

*Screenshot*
![skillset2](http://i.imgur.com/m3qTl6g.png)