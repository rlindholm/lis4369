# LIS4369

## Assignment 1 Requirements:
Four Parts

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions


-README.md file should include the following items  
-screenshot of hwapp application running  
-screenshot of aspcorenetapp application running My .NET Core Installation  
-Git commands and short descriptions  



*part 1*

git init - create a new local repository  
git status - check the status of files in staging  
git add - add files for staging  
git commit - commit changes to head but not to the local repository  
git push - send changes to the master branch of the remote repository  
git pull - pull changes down to local repository from the remote repository  
git clone - create a working copy of a local repository  

*Assignment Screenshots*

![Screenshot of aspnetcoreapp application running My .NET Core Installation] (http://i.imgur.com/eDwyt8v.png)

![Screenshot of hwapp application running] (http://i.imgur.com/DJ4gOcs.png)