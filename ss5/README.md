# Skillset 5

##Program requirements

1) Initialize suitable variables(s);  
2) Prompt and capture user input;  
3) Perform the mathematical operations on the input;  
4) Allow user to press any key to return back to command line;  

*Screenshot*  
![skillset5](http://i.imgur.com/fc6QZbt.png)