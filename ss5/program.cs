﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Title: Non-Value Returning (void) Functions");
            Console.WriteLine("Author: Charles Ryan Lindholm");
            DateTime date = DateTime.Now;
            Console.WriteLine("Now: " + date);

            Console.Write("num1: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("num2: ");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("\n1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplication");
            Console.WriteLine("4 - Division");
	    Console.WriteLine("5 - Exponentiation");

            Console.Write("Choose a mathematical operation: ");
            int choice = int.Parse(Console.ReadLine());

            if (choice == 1)
            {
		Addition(num1, num2);
            }
            else if (choice == 2)
            {
		Subtraction(num1, num2);
            }
            else if (choice == 3)
            {
		Multiplication(num1, num2);
            }
            else if (choice == 4)
	    {
		Division (num1, num2);
            }
	    else if (choice == 5)
	    {
		Exponentiation (num1, num2);
	    }
            else
                Console.WriteLine("Invalid choice");
	    Console.WriteLine("Press any key to return to the command line");
            Console.ReadKey();
        }
	
	public static void Addition (int num1, int num2)
	{
		int add = num1 + num2;
		Console.WriteLine("*** Result of Addition operation ***");
		Console.WriteLine(add);
	}	
       	public static void Subtraction (int num1, int num2)
	{
		int subtract = num1 + num2;
		Console.WriteLine("*** Result of Subtraction operation ***");
		Console.WriteLine(subtract);
	}
	public static void Multiplication (int num1, int num2)
	{
                Console.WriteLine("*** Result of Multiplication operation: ***");
                int multiplication = num1 * num2;
                Console.WriteLine(multiplication);
            }
	public static void Division (int num1, int num2)
	{
             while (num1 == 0 || num2 == 0)
             {
                 Console.Write("Division by zero is not permitted please enter a new number: ");
                 if (num1 == 0)
                     num1 = int.Parse(Console.ReadLine());
                 else
                     num2 = int.Parse(Console.ReadLine());
                }
             Console.WriteLine("*** Result of Division operation: ***");
             double division = (double)num1 / (double)num2;
             Console.WriteLine(division);
	}
	public static void Exponentiation (int num1, int num2)
	{
		Console.WriteLine("*** Result of Exponentiation operation: ***");
		double exponentiation = Math.Pow((double)num1, (double)num2);
		Console.WriteLine(exponentiation);
	}

    }
}
