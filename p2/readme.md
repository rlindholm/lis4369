# LIS4369

#Project 2 Requirements:
Using LINQ

1. Display assignment requirements  
2. Display your name as author  
3. Display current date/time (must include date/time, your format preference)   
4. Work your way through the tutorial  
5. Create the required searches  

Valid Input Screenshots  
![Valid Input]  (http://i.imgur.com/oaR0bQl.png)
![Valid Input]  (http://i.imgur.com/eNvj6pH.png)  
![Valid Input]  (http://i.imgur.com/2EiZ9nB.png)

Invalid Input  
![Invalid Input](http://i.imgur.com/OhESHDl.png)
