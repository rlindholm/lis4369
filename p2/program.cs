﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
       public static void Main()
       {
	string requirements = 
@"Program Requirements:
Using LINQ (Language Integrated Query)
Author: Mark K. Jowett

*After* completing the required tutorial create the following program:

1) Prompt the user for last name, return full name, occupation and age.

2) Prompt the user for age and occupation (Dev or Manager). Return full name.
   (*Must include data validation on numeric data.)
3) Allow user to press any key to return back to command line.";

   	    Console.WriteLine(requirements);

            var people = GenerateListOfPeople();

     	    Console.WriteLine("\n***Finding Items in Collections***");
        
	    var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
	    Console.WriteLine("\nWhere: ");
	    foreach(var person in peopleOverTheAgeOf30)
		    {
				Console.WriteLine(person.FirstName);
		    }

            Console.WriteLine("\nSkip: ");
	    IEnumerable<Person> afterTwo = people.Skip(2);
	    foreach(var person in afterTwo)
	    {
		Console.WriteLine(person.FirstName);
	    }
	    
	    Console.WriteLine("\nTake: ");
	    IEnumerable<Person> takeTwo = people.Take(2);
	    foreach (var person in takeTwo)
	    {
		Console.WriteLine(person.FirstName);
	    }



	    Console.WriteLine("\n***Changing Each Item in Collections ****\n");

	    Console.WriteLine("Select: ");
	    IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
	    foreach(var firstName in allFirstNames)
	    {
		Console.WriteLine(firstName);
	    }

	    Console.WriteLine("\nFullName class and objects: ");
	    IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
	    foreach (var fullName in allFullNames)
	    {
		Console.WriteLine($"{fullName.Last}, {fullName.First}");
	    }
	    
	    Console.WriteLine("\n***Finding One Item in Collections***");
	   
	    Console.WriteLine("\nFirstOrDefault: ");
	    Person firstOrDefault = people.FirstOrDefault();
	    Console.WriteLine(firstOrDefault.FirstName);

	    Console.WriteLine("\nFirstOrDefault as filter: ");
	    var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age > 30);
            var firstThirtyYearOld2 = people.Where(x => x.Age > 30).FirstOrDefault();
            Console.WriteLine(firstThirtyYearOld1.FirstName);
            Console.WriteLine(firstThirtyYearOld2.FirstName);

	    Console.WriteLine("\nHow OrDefault works: ");
	    List<Person> emptyList = new List<Person>();
            Person willBeNull = emptyList.FirstOrDefault();
            List<Person> people1 = GenerateListOfPeople();
            Person willAlsoBeNull = people1.FirstOrDefault(x => x.FirstName == "John"); 
            Console.WriteLine(willBeNull == null);
            Console.WriteLine(willAlsoBeNull == null);

	    Console.WriteLine("\nLastOrDefault as filter: ");
            Person lastOrDefault = people.LastOrDefault();
            Console.WriteLine(lastOrDefault.FirstName);
            Person lastThirtyYearOld = people.LastOrDefault(x => x.Age > 30);
            Console.WriteLine(lastThirtyYearOld.FirstName);
	    
	    Console.WriteLine("\nSingleOrDefault as filter: ");
	    Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
            Console.WriteLine(single.FirstName);

	   Console.WriteLine("\n***Finding Data about Collections***");
	   Console.WriteLine("\nCount(): ");
           int numberOfPeopleInList = people.Count();
           Console.WriteLine(numberOfPeopleInList);

	   Console.WriteLine("\nCount() with predicate expression: ");
	   int peopleOverTwentyFive = people.Count(x => x.Age > 25);
           Console.WriteLine(peopleOverTwentyFive);

	   Console.WriteLine("\nAny(): ");
	   bool thereArePeople = people.Any();
           Console.WriteLine(thereArePeople);
           bool thereAreNoPeople = emptyList.Any();
           Console.WriteLine(thereAreNoPeople);
	   
	   Console.WriteLine("\nAll(): ");
	   bool allDevs = people.All(x => x.Occupation == "Dev");
           Console.WriteLine(allDevs);
           bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
           Console.WriteLine(everyoneAtLeastTwentyFour);

	   Console.WriteLine("\n***Converting Results to Collections***");
	   Console.WriteLine("\nToList(): ");
	   List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
	   foreach(var person in listOfDevs)
	   {
		Console.WriteLine(person.FirstName + " " + person.LastName);
	   }
	   

	   Console.WriteLine("\nToArray(): ");
	   Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray();
	   foreach (var person in arrayOfDevs)
	   {
		Console.WriteLine(person.FirstName + " " + person.LastName);
	   }
	   Console.WriteLine("\n***Required Program***");
	   
	   Console.WriteLine("\nNote: Searches are case sensitive.");

	   Console.WriteLine("Please Enter last name: ");
	   string search = Console.ReadLine();	   	   
	   Person searchResults = people.SingleOrDefault(x => x.LastName == search);
	   Console.WriteLine(searchResults.FirstName + " " + searchResults.LastName + " is a " + searchResults.Occupation + " and is " + searchResults.Age + " years old.");
	   
	   int searchAge;
	   Console.Write("Please enter an age: ");
	   if (!int.TryParse(Console.ReadLine(), out searchAge))
	   {
		Console.Write("Age must be an integer: ");
	   }
	   
	   Console.Write("Please enter an occupation: ");
	   string searchOccupation = Console.ReadLine();
	   var peoplesAge = people.Where (x => x.Age == searchAge);

	   searchResults = peoplesAge.SingleOrDefault(x => x.Occupation == searchOccupation);
  	   Console.WriteLine(searchResults.FirstName + " " + searchResults.LastName);
	   	   
	}

public static List<Person> GenerateListOfPeople()
{
	var people = new List<Person>();
	people.Add(new Person {FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24});
	people.Add(new Person {FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
	people.Add(new Person {FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 38 });
	people.Add(new Person {FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
	people.Add(new Person {FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });
	return people;
}
}    


public class Person
{
	public string FirstName { get; set; }
	public string LastName { get; set; }
	public string Occupation { get; set; }
	public int Age { get; set; }
}

public class FullName
{
	public string First { get; set; }
	public string Last { get; set; }
}