﻿using System;

namespace ss7
{
    public class Program
    {
        public static void Main(string[] args)
        {
	    Console.WriteLine("\nTitle: Skillset7 - using classes");
	    Console.WriteLine("Author: Ryan Lindholm");
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss tt\n"));
	    Person p1 = new Person();
	    Console.WriteLine("First Name: " + p1.GetFName());
	    Console.WriteLine("Last Name: " + p1.GetLName());	    

	    Console.Write("\nModify default constructor object's data member values:\nUse setter/getter methods:");

	    Console.Write("\nFirst Name: ");
	    string fname = Console.ReadLine();
	    Console.Write("Last Name: ");
	    string lname = Console.ReadLine();
	    
	    p1.SetFName(fname);
	    p1.SetLName(lname);

	    string newfname = p1.GetFName();
	    string newlname = p1.GetLName();
	    
	    Console.WriteLine("\nDisplay object's new data member values: ");
	    Console.WriteLine("First Name: " + newfname);
	    Console.WriteLine("Last Name: " + newlname + "\n");


	    Console.WriteLine("Call parameterized constructor (accepts two arguments)");
	    Console.Write("First Name: ");
	    fname = Console.ReadLine();
	    Console.Write("Last Name: ");
	    lname = Console.ReadLine();
	    Console.Write("\n");
	    
	    Person p2 = new Person(fname, lname);
	    Console.WriteLine("Full name: " + p2.GetFName() + " " + p2.GetLName() + "\n");
	    Console.WriteLine("Press any key to exit program");
	    Console.ReadKey();
	    

        }
    }
}
