# Skillset 7

##Program requirements

- Creating a class and using default and parameterized constructors
- Modify objects member data using getter and setter methods

*Screenshot*
![skillset7](http://i.imgur.com/WStQitz.png)