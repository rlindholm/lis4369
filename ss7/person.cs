using System;

namespace ss7
{
	public class Person
	{
		private string fname;
		private string lname;


		public Person()
		{
			fname = "John";
			lname = "Doe";
			
			Console.WriteLine("Creating default person object from default constructor (accepts no arguments): ");

		}
		
		public Person(string fn="", string ln="")
		{
			fname = fn;
			lname = ln;
			
			Console.WriteLine("Creating Person object from within paramatized constructor");

		}

		public void SetFName (string fn="")
		{
			fname = fn;
		}
		public void SetLName (string ln="")
		{
			lname = ln;
		}


		public string GetFName ()
		{
			return fname;
		}
		public string GetLName ()
		{
			return lname;
		}



	}



}