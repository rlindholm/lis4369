﻿using System;

namespace a5
{
    public class Program
    {
        public static void Main(string[] args)
        {
		
	    Console.WriteLine("\nTitle: A5 - Using Class Inheritance");
	    Console.WriteLine("Also, Overloading (Compile-Time Polymorphism) and Overriding (Run-Time Polymorphism)");
	    Console.WriteLine("Author: Ryan Lindholm");
	    string requirements = @"////////////////////
Program Requirements:
1) Create Vehicle (base/super/parent) class

1) Create two private data members (fields/instance variable):
a. milesTraveled (float)
b. gallonsUsed (float)

2) Create four properties:
a. Manufacturer (string)
b. Make (string)
c. Model (string)
d. MPG (float)

*Note*: Only Manufacterer, Make, and Model are auto-implemented properties (MPG is read-only)

3) Create two setter/mutator methods:
a. SetMiles
b. SetGallons

4) Create four getter/accessor methods:
a. GetMiles
b. GetGallons
c. GetObjectInfo()
d. GetObjectInfo(arg) (overloaded method): accepts string arg to be used as a seperator for display purposes

5) Create two constructors
a. default constructor (accepts no args)
b. parameterized constructor w/ default parameter values that accepts three arguments

B) Create Car (derived/sub/child)

a) Create appropriate data members, properties and constructors
b) Create appropriate getter and accessor methods

C) Create three vehicle objects:
a. one from default constructor, display data member values
b. two from parameterized constructors

D) Instantiate one car object:
a. From parameterized constructor

E) Include Data Validation

F) Allow user to press any key to return to command line


/////////////////////////////////";

            Console.WriteLine(requirements);
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss tt\n"));
	    
	    Console.WriteLine();

	    Console.WriteLine("\nVehicle1 - instantiating new object from default constructor)");	    	    
	    Vehicle vehicle1 = new Vehicle();

	    Console.WriteLine(vehicle1.GetObjectInfo());

	    Console.WriteLine("\nvehicle2 (user input): Call parameterized base constructor (accepts arguments):");

	    string p_manufacturer = "";
	    string p_make = "";
	    string p_model = "";
	    float p_miles = 0.0f;
	    float p_gallons = 0.0f;
	    string p_seperator = "";

	    Console.Write("Manufacturer: ");
	    p_manufacturer = Console.ReadLine();

	    Console.Write("Make: ");
	    p_make = Console.ReadLine();

	    Console.Write("Model: ");
	    p_model = Console.ReadLine();

	    Console.Write("Miles driven: ");
	    while (!float.TryParse(Console.ReadLine(), out p_miles))
	    {
		Console.Write("Miles must be float: ");
	    }

	    Console.Write("Gallons Used: ");
	    while (!float.TryParse(Console.ReadLine(), out p_gallons))
	    {
		Console.Write("Gallons must be a float: ");
	    }

	    Console.WriteLine("\nVehicle2 - (Instantiating new object, passing *only 1st arg*)");

	    Vehicle vehicle2 = new Vehicle(p_manufacturer);

	    Console.WriteLine(vehicle2.GetObjectInfo());

	    Console.WriteLine("\nUser input: vehicle 2 - passing arg to overloaded GetObjectInfo(arg): ");

	    Console.Write("Delimiter: ");
	    p_seperator = Console.ReadLine();
	    
	    Console.WriteLine(vehicle2.GetObjectInfo(p_seperator));

	    Console.WriteLine("\nvehicle3 - insantiating new object passing all vehicle args");

	    Vehicle vehicle3 = new Vehicle(p_manufacturer, p_make, p_model);

	    vehicle3.SetGallons(p_gallons);
	    vehicle3.SetMiles(p_miles);


	    Console.WriteLine("\nUser input: vehicle3 - passing arg to overloaded GetObjectInfo(arg)");

	    Console.Write("Delimiter: ");
	    p_seperator = Console.ReadLine();

	    Console.WriteLine(vehicle3.GetObjectInfo(p_seperator));

	    Console.WriteLine("\nDemonstrating Polymorphism");
	    Console.WriteLine("User input: car1 - calling parameterized base class constructor explicitely\n");

	    string p_style = "";

	    Console.Write("Manufacturer: ");
	    p_manufacturer = Console.ReadLine();	    

	    Console.Write("Make: ");
	    p_make = Console.ReadLine();

	    Console.Write("Model: ");
	    p_model = Console.ReadLine();

	    Console.Write("Miles driven: ");
	    while (!float.TryParse(Console.ReadLine(), out p_miles))
	    {
		Console.Write("Miles must be float: ");
	    }

	    Console.Write("Gallons Used: ");
	    while (!float.TryParse(Console.ReadLine(), out p_gallons))
	    {
		Console.Write("Gallons must be a float: ");
	    }

	    Console.Write("Style: ");
	    p_style = Console.ReadLine();

	    Console.WriteLine("\ncar1 - instantiating new derived object with args");
	    Car car1 = new Car(p_manufacturer, p_make, p_model, p_style);
	    car1.SetGallons(p_gallons);
	    car1.SetMiles(p_miles);

	    Console.WriteLine("\nUser input: car1 passing arg to overridden GetObjectInfo(arg)");
	    Console.Write("Delimiter: ");

	    p_seperator = Console.ReadLine();
	    Console.WriteLine(car1.GetObjectInfo(p_seperator));

	    Console.WriteLine();
	    Console.WriteLine("\nPress any key to exit!");
	    Console.ReadKey();

        }
    }
}
