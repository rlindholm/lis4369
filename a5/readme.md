# LIS4369

#Assignment 5 Requirements:
Using Class Inheritance

1. Display assignment requirements  
2. Display your name as author  
3. Display current date/time (must include date/time, your format preference)   
4. Create the Vehicle and Car classes with the appropriate data members, properties and methods  
5. Instantiate class objects and access member data  
6. Include data validation  

Valid Input Screenshots  
![Valid Input](http://i.imgur.com/giUIm6z.png)  
![Valid Input](http://i.imgur.com/lClWxAf.png)  

Invalid Input  
![Invalid Input](http://i.imgur.com/j6r76Pb.png)  
